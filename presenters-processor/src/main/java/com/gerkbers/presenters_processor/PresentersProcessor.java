package com.gerkbers.presenters_processor;

import com.gerkbers.presenters_annotation.GenerateViewComponent;
import com.gerkbers.presenters_annotation.PackageForComponent;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;
import javax.tools.StandardLocation;

import sun.reflect.generics.tree.Tree;

/**
 * Created by k.bersenev on 10.05.2018.
 * com.gerkbers.presenters_processor
 */
@SupportedAnnotationTypes({"com.gerkbers.presenters_annotation.GenerateViewComponent",
"com.gerkbers.presenters_annotation.PackageForComponent"})
public class PresentersProcessor extends AbstractProcessor {

    private Filer filer;
    private Messager messager;
    private Elements elements;
    private int sizeParents = 0;
    private PackageElement parentComponentPackage;
    private Utils utils;

    private Tree tree;

    private final List<ComponentModuleView> uiComponentModulePairs = new ArrayList<>();

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        filer = processingEnv.getFiler();
        messager = processingEnv.getMessager();
        elements = processingEnv.getElementUtils();
        utils = new Utils(messager, filer);
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if(!loadParentPackage(roundEnv)) return false;

        createUiComponents(roundEnv);

        UIComponentBuilder.create(uiComponentModulePairs, parentComponentPackage, messager, utils);
        return true;
    }

    private boolean loadParentPackage(RoundEnvironment roundEnv) {
        Set<? extends Element> parents = roundEnv.getElementsAnnotatedWith(PackageForComponent.class);
        sizeParents += parents.size();
        if(sizeParents != 1) {
            messager.printMessage(Diagnostic.Kind.ERROR, "View component parent must be one, but it is " + sizeParents);
            return false;
        }

        for(Element element: parents) {
            if(element.getKind() != ElementKind.PACKAGE) {
                messager.printMessage(Diagnostic.Kind.ERROR, "Annotation \"PackageForComponent\" must be used for package");
            }
            parentComponentPackage = (PackageElement) element;
        }
        return true;
    }

    private void createUiComponents(RoundEnvironment roundEnv) {
        for(Element element : roundEnv.getElementsAnnotatedWith(GenerateViewComponent.class)){
            if(element.getKind() != ElementKind.CLASS) {
                messager.printMessage(Diagnostic.Kind.ERROR, "Annotation GenerateViewComponent must be used for class");
                return;
            }
            messager.printMessage(Diagnostic.Kind.NOTE, "Create UI component");
            TypeElement typeElement = (TypeElement) element;
            uiComponentModulePairs.add(
                    ComponentBuilder.create(elements, typeElement, utils)
            );
        }
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_8;
    }

}
