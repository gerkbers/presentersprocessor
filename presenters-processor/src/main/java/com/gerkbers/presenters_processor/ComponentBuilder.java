package com.gerkbers.presenters_processor;

import com.gerkbers.presenters_annotation.GenerateViewComponent;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Constructor;
import java.util.List;

import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.inject.Scope;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ErrorType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.IntersectionType;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.NoType;
import javax.lang.model.type.NullType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.TypeVisitor;
import javax.lang.model.type.UnionType;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;

/**
 * Created by k.bersenev on 11.05.2018.
 * com.gerkbers.presenters_processor
 */

public class ComponentBuilder {

    private String packageName;
    private TypeName viewName;
    private TypeName presenterClassName;
    private String presenterShortName;
    private Utils utils;
    private String packageViewName;

    private List<? extends VariableElement> presenterConstructorParameters;

    public static ComponentModuleView create(Elements elements, TypeElement typeElement, Utils utils) {
        ComponentBuilder componentBuilder = new ComponentBuilder(elements, typeElement, utils);
        return componentBuilder.create();
    }

    private ComponentModuleView create() {

        TypeSpec annotationScope = createScopeForPresenter();
        TypeSpec moduleClass = createModuleForPresenter(annotationScope);
        TypeSpec componentInterface = createComponentForPresenter(annotationScope, moduleClass);

        TypeName componentInterfaceName = ClassName.get(packageName, componentInterface.name);
        TypeName moduleClassName = ClassName.get(packageName, moduleClass.name);
        return new ComponentModuleView(viewName, componentInterfaceName, moduleClassName);
    }

    private ExecutableElement findConstructor(TypeMirror classTypeMirror) {
        for(Element method: ((DeclaredType)classTypeMirror).asElement().getEnclosedElements()) {
            if(method instanceof ExecutableElement && method.getSimpleName().toString().equals("<init>")) {
                return (ExecutableElement)method;
            }
        }
        return null;
    }


    private ComponentBuilder(Elements elements, TypeElement viewTypeElement, Utils utils) {
        this.utils = utils;
        packageViewName = elements.getPackageOf(viewTypeElement).getQualifiedName().toString();
        packageName = packageViewName + ".di";
        viewName = ClassName.get(viewTypeElement);



        try {
            viewTypeElement.getAnnotation(GenerateViewComponent.class).presenter();
        }
        catch(MirroredTypeException e) {
            presenterClassName = TypeName.get(e.getTypeMirror());
            TypeMirror typeMirror = e.getTypeMirror();

            ExecutableElement constructorElement = findConstructor(typeMirror);
            if(constructorElement != null)
                presenterConstructorParameters = constructorElement.getParameters();

        }
        presenterShortName = viewTypeElement.getSimpleName().toString();
    }

    private TypeSpec createScopeForPresenter() {
        TypeSpec.Builder scopeAnnotation =
                TypeSpec.annotationBuilder(presenterShortName + "Scope");

        scopeAnnotation.addAnnotation(Scope.class);

        AnnotationSpec retentionAnnotation =
                AnnotationSpec.builder(Retention.class)
                .addMember("value", "java.lang.annotation.RetentionPolicy.$L", RetentionPolicy.RUNTIME)
                .build();
        scopeAnnotation.addAnnotation(retentionAnnotation);
        TypeSpec annotation = scopeAnnotation.build();
        utils.saveFile(annotation, packageName);
        return annotation;
    }

    private TypeSpec createModuleForPresenter(TypeSpec annotationScope) {
        TypeSpec.Builder moduleClass =
                TypeSpec.classBuilder(presenterShortName + "Module")
                .addModifiers(Modifier.PUBLIC);

        moduleClass.addAnnotation(Module.class);

        MethodSpec.Builder provideMethodBuilder = MethodSpec
                .methodBuilder("providePresenter")
                .addAnnotation(Provides.class)
                .addAnnotation(ClassName.get(packageName, annotationScope.name))
                .returns(presenterClassName);


        StringBuilder presenterParameters = new StringBuilder();
        if(presenterConstructorParameters != null) {
            for(VariableElement parameter: presenterConstructorParameters) {
                String name = "obj" + parameter.getSimpleName();
                provideMethodBuilder.addParameter(ClassName.get(parameter.asType()), name);
                if(presenterParameters.toString().equals("")) {
                    presenterParameters = new StringBuilder(name);
                }
                else {
                    presenterParameters.append(", ").append(name);
                }
            }
        }
        provideMethodBuilder.addStatement("return new $T($N)", presenterClassName, presenterParameters.toString());
        moduleClass.addMethod(provideMethodBuilder.build());
        TypeSpec moduleClassSpec = moduleClass.build();
        utils.saveFile(moduleClassSpec, packageName);
        return moduleClassSpec;
    }

    private TypeSpec createComponentForPresenter(TypeSpec annotationScope, TypeSpec moduleClass) {

        TypeSpec.Builder componentInterface =
                TypeSpec.interfaceBuilder(presenterShortName + "Component")
                        .addModifiers(Modifier.PUBLIC);

        AnnotationSpec annotationSubcomponent =
                AnnotationSpec.builder(Subcomponent.class)
                        .addMember("modules", moduleClass.name + ".class")
                        .build();

        componentInterface.addAnnotation(annotationSubcomponent);

        componentInterface.addAnnotation(ClassName.get(packageName, annotationScope.name));

        MethodSpec injectMethod = MethodSpec
                .methodBuilder("inject")
                .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
                .addParameter(viewName, "fragment")
                .build();

        componentInterface.addMethod(injectMethod);
        TypeSpec componentInterfaceSpec = componentInterface.build();
        utils.saveFile(componentInterfaceSpec, packageName);
        return componentInterfaceSpec;
    }



}
