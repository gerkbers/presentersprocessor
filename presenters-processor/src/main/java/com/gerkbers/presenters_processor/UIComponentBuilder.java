package com.gerkbers.presenters_processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.swing.tree.TreePath;

import dagger.Subcomponent;
import sun.reflect.generics.tree.Tree;

/**
 * Created by k.bersenev on 11.05.2018.
 * com.gerkbers.presenters_processor
 */

public class UIComponentBuilder {

    private List<ComponentModuleView> componentModuleList;
    private PackageElement parentPackage;
    private Messager messager;
    private Utils utils;

    private UIComponentBuilder(List<ComponentModuleView> componentModuleList, PackageElement parentPackage, Messager messager, Utils utils) {
        this.componentModuleList = componentModuleList;
        this.parentPackage = parentPackage;
        this.messager = messager;
        this.utils = utils;
    }

    public static void create(List<ComponentModuleView> componentModuleList, PackageElement parentPackage, Messager messager, Utils utils) {
        UIComponentBuilder uiComponentBuilder = new UIComponentBuilder(componentModuleList, parentPackage, messager, utils);
        uiComponentBuilder.create();
    }

    private void create() {
        TypeSpec uiComponentInterface = createUIComponent();
        createUiComponentsHolder(uiComponentInterface);
    }

    private TypeSpec createUIComponent() {
        TypeSpec.Builder uiComponentInterfaceBuilder =
                TypeSpec.interfaceBuilder("UiComponent")
                .addModifiers(Modifier.PUBLIC);

        uiComponentInterfaceBuilder.addAnnotation(Subcomponent.class);

        for(ComponentModuleView pair: componentModuleList) {
            MethodSpec methodSpec =
                    MethodSpec.methodBuilder("plus")
                            .addModifiers(Modifier.ABSTRACT, Modifier.PUBLIC)
                            .addParameter(pair.getModule(), "module")
                            .returns(pair.getComponent())
                            .build();
            uiComponentInterfaceBuilder.addMethod(methodSpec);
        }
        TypeSpec uiComponentInterface = uiComponentInterfaceBuilder.build();
        utils.saveFile(uiComponentInterface, parentPackage.getQualifiedName().toString());
        return uiComponentInterface;
    }

    private void createUiComponentsHolder(TypeSpec uiComponentInterface) {
        TypeSpec.Builder uiComponentHolderBuilder =
                TypeSpec.classBuilder("UiComponentsHolder")
                .addModifiers(Modifier.PUBLIC);

        FieldSpec mapUiComponents = FieldSpec.builder(ParameterizedTypeName.get(Map.class, Class.class, Object.class), "mapUiComponents", Modifier.PRIVATE, Modifier.FINAL)
                .initializer("new $T<>()", HashMap.class)
                .build();

        uiComponentHolderBuilder.addField(mapUiComponents);

        TypeName uiComponentName = ClassName.get(parentPackage.getQualifiedName().toString(), uiComponentInterface.name);

        FieldSpec uiComponent = FieldSpec.builder(uiComponentName, "uiBaseComponent", Modifier.PRIVATE)
                .build();

        uiComponentHolderBuilder.addField(uiComponent);

        MethodSpec constructor = MethodSpec.constructorBuilder()
                .addModifiers(Modifier.PUBLIC)
                .addParameter(uiComponentName, "uiBaseComponent")
                .addStatement("this.uiBaseComponent = uiBaseComponent")
                .build();

        uiComponentHolderBuilder.addMethod(constructor);

        MethodSpec.Builder injectMethodBuilder = MethodSpec.methodBuilder("inject")
                .addModifiers(Modifier.PUBLIC)
                .returns(TypeName.BOOLEAN)
                .addParameter(Object.class, "view");

        boolean beginControlFlow = true;
        for(ComponentModuleView pair : componentModuleList) {
            TypeName component = pair.getComponent();
            TypeName module = pair.getModule();
            TypeName view = pair.getView();
            if(beginControlFlow) {
                beginControlFlow = false;
                injectMethodBuilder.beginControlFlow("if(view instanceof $T)", view);
            }
            else {
                injectMethodBuilder.nextControlFlow("else if(view instanceof $T)", view);
            }

            injectMethodBuilder.addStatement(
                    "$T component = ($T)mapUiComponents.get($T.class)", component, component, view
            )
                    .beginControlFlow("if(component == null)")
                    .addStatement("component = uiBaseComponent.plus(new $T())", module)
                    .addStatement("mapUiComponents.put($T.class, component)", view)
                    .endControlFlow()
                    .addStatement("component.inject(($T)view)", view)
                    .addStatement("return true");
        }

        if(!beginControlFlow) {
            injectMethodBuilder.endControlFlow();
        }
        injectMethodBuilder.addStatement("return false");

        uiComponentHolderBuilder.addMethod(injectMethodBuilder.build());

        MethodSpec releaseMethod =
                MethodSpec.methodBuilder("release")
                .addModifiers(Modifier.PUBLIC)
                .addParameter(Object.class, "view")
                .addStatement("mapUiComponents.remove(view.getClass())")
                .build();

        uiComponentHolderBuilder.addMethod(releaseMethod);

        utils.saveFile(uiComponentHolderBuilder.build(), parentPackage.getQualifiedName().toString());
    }
}
