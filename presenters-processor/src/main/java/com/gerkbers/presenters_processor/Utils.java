package com.gerkbers.presenters_processor;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeSpec;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

/**
 * Created by k.bersenev on 18.05.2018.
 * com.gerkbers.presenters_processor
 */

public class Utils {

    private Messager messager;
    private Filer filer;

    public Utils(Messager messager, Filer filer) {
        this.messager = messager;
        this.filer = filer;
    }

    public void saveFile(TypeSpec file, String packageName) {
        try{
            JavaFile javaFile = JavaFile.builder(packageName, file)
                    .build();
            FileObject src = filer.getResource(StandardLocation.SOURCE_OUTPUT, packageName, file.name);
            if(src != null) {
                src.delete();
                messager.printMessage(Diagnostic.Kind.WARNING, "src non null");
            }
            javaFile.writeTo(filer);

        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    private String getPath()throws IOException {
        JavaFileObject generationForPath = filer.createSourceFile("PathFor" + getClass().getSimpleName());
        Writer writer = generationForPath.openWriter();
        String sourcePath = generationForPath.toUri().getPath();
        writer.close();
        generationForPath.delete();

        return sourcePath;
    }

}
