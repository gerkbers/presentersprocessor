package com.gerkbers.presenters_processor;

import com.squareup.javapoet.TypeName;

/**
 * Created by k.bersenev on 18.05.2018.
 * com.gerkbers.presenters_processor
 */

public class ComponentModuleView {

    private TypeName view;
    private TypeName component;
    private TypeName module;

    public ComponentModuleView(TypeName view, TypeName component, TypeName module) {
        this.view = view;
        this.component = component;
        this.module = module;
    }

    public TypeName getComponent() {
        return component;
    }

    public TypeName getModule() {
        return module;
    }

    public TypeName getView() {
        return view;
    }
}
