package com.gerkbers.presenters_annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by k.bersenev on 18.05.2018.
 * com.gerkbers.presenters_annotation
 *
 * This annotation must be used in package info
 * where UiComponent and UiComponentsHolder will be generate.
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.PACKAGE)
public @interface PackageForComponent {
}
