package com.gerkbers.presenters_annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by k.bersenev on 10.05.2018.
 * com.gerkbers.presenters_annotation
 *
 * This annotation can be used on your view(activity, fragment and others).
 * Annotation processor will generate component, module, scope for this view.
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface GenerateViewComponent {

    /**
     *
     * Presenter which communicate to your view
     */
    Class<?> presenter();
}
