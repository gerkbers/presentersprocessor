package com.gerkbers.presenters_processor_example.dependency_injection.app

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope