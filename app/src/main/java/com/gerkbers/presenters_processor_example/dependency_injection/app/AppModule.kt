package com.gerkbers.presenters_processor_example.dependency_injection.app

import com.gerkbers.presenters_processor_example.dependency_injection.UiComponentsHolder
import com.gerkbers.presenters_processor_example.repositories.ExampleRepository
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    @AppScope
    fun provideExampleRepository() = ExampleRepository()
}