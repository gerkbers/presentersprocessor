package com.gerkbers.presenters_processor_example.dependency_injection.app

import com.gerkbers.presenters_processor_example.dependency_injection.UiComponent
import dagger.Component

@AppScope
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun createUiComponent(): UiComponent
}