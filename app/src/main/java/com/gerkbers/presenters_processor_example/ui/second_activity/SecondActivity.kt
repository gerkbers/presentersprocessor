package com.gerkbers.presenters_processor_example.ui.second_activity

import android.os.Bundle
import com.gerkbers.presenters_annotation.GenerateViewComponent
import com.gerkbers.presenters_processor_example.R
import com.gerkbers.presenters_processor_example.ui.base.BaseAutodependencyActivity

@GenerateViewComponent(presenter = SecondPresenter::class)
class SecondActivity : BaseAutodependencyActivity<SecondView, SecondPresenter>(), SecondView {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
    }
}
