package com.gerkbers.presenters_processor_example.ui.main

interface MainState {

    data class ContentState(val description: String): MainState
}