package com.gerkbers.presenters_processor_example.ui.main

import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.Observable

interface MainView: MvpView {

    fun render(state: MainState)

    fun loadIntent(): Observable<Any>
}