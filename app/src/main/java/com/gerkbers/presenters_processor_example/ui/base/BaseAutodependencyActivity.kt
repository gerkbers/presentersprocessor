package com.gerkbers.presenters_processor_example.ui.base

import android.os.Bundle
import com.gerkbers.presenters_processor_example.app.App
import com.hannesdorfmann.mosby3.mvi.MviActivity
import com.hannesdorfmann.mosby3.mvi.MviPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import javax.inject.Inject

abstract class BaseAutodependencyActivity<V: MvpView, P: MviPresenter<V, *>>: MviActivity<V, P>() {


    @Inject lateinit var presenter: P

    override fun createPresenter(): P {
        return presenter
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        App.uiComponentsHolder.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        if(isFinishing) {
            App.uiComponentsHolder.release(this)
        }
    }
}