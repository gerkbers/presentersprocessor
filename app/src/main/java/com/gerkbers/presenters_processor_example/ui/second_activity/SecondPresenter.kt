package com.gerkbers.presenters_processor_example.ui.second_activity

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter

class SecondPresenter: MviBasePresenter<SecondView, SecondState>() {
    override fun bindIntents() {

    }
}