package com.gerkbers.presenters_processor_example.ui.main

import com.gerkbers.presenters_processor_example.repositories.ExampleRepository
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers

class MainPresenter(
    private val exampleRepository: ExampleRepository
): MviBasePresenter<MainView, MainState>() {


    override fun bindIntents() {

        val observable = intent(MainView::loadIntent)
            .switchMap{ exampleRepository.exampleContent() }
            .map<MainState>(MainState::ContentState)
            .observeOn(AndroidSchedulers.mainThread())

        subscribeViewState(observable, MainView::render)
    }
}