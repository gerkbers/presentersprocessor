package com.gerkbers.presenters_processor_example.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gerkbers.presenters_annotation.GenerateViewComponent
import com.gerkbers.presenters_processor_example.R
import com.gerkbers.presenters_processor_example.ui.base.BaseAutodependencyActivity
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.*

@GenerateViewComponent(presenter = MainPresenter::class)
class MainActivity : BaseAutodependencyActivity<MainView, MainPresenter>(), MainView {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun render(state: MainState) {
        when(state){
            is MainState.ContentState -> renderContentState(state)
        }
    }

    private fun renderContentState(state: MainState.ContentState) {
        descriptionTextView.text = state.description
    }

    override fun loadIntent(): Observable<Any> {
        return Observable.just(Any())
    }
}
