package com.gerkbers.presenters_processor_example.app

import android.app.Application
import com.gerkbers.presenters_processor_example.dependency_injection.UiComponentsHolder
import com.gerkbers.presenters_processor_example.dependency_injection.app.AppComponent
import com.gerkbers.presenters_processor_example.dependency_injection.app.DaggerAppComponent

class App: Application() {

    companion object{
        /**
         * This instance of generated class help to inject dependencies
         * to your views(where @GenerateViewComponent is used).
         * Use inject() in base classes of views to inject all dependencies.
         * Use release() in base classes of views when view will be destroying.
         *
         * UiComponent instance can be created in component(like RepositoryComponent),
         * will depend on this component and will provide presenters and views
         * by instances. Presenters will get instances which classes is declared
         * in presenter's constructors.
         */
        lateinit var uiComponentsHolder: UiComponentsHolder
    }

    override fun onCreate() {
        super.onCreate()
        val appComponent = DaggerAppComponent.create()

        uiComponentsHolder = UiComponentsHolder(appComponent.createUiComponent())
    }
}