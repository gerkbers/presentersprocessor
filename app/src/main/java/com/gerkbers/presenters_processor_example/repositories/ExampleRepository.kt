package com.gerkbers.presenters_processor_example.repositories

import io.reactivex.Observable

class ExampleRepository {

    fun exampleContent(): Observable<String>{
        return Observable.just("Hello from repository!!")
    }
}